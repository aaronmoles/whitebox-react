import axios from "axios";
import {URL_WS_ADD_PLAYER} from "../../ws/Endpoint";
import {generateRoute} from "../../routing/Routing";
import Player from "../../Entity/Player";
import {push} from "react-router-redux";
import {ROUTE_TEAM} from "../../routing/Routes";

export const savePlayer = (playerData: Player) => (dispatch: any) => {

    axios.put(URL_WS_ADD_PLAYER, playerData)
        .then(res => {
            console.log('ADDED');
            dispatch(push(generateRoute(ROUTE_TEAM, playerData.club_id)));
        });
};