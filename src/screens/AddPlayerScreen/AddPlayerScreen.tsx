import * as React from "react";
import Screen from "../Screen.tsx";
import {bindActionCreators} from "redux";
import * as addPlayerActions from './AddPlayerActions';
import {connect} from "react-redux";

const mapDispatchToProps = (dispatch: any) => ({
    addPlayerActions: bindActionCreators(addPlayerActions as any, dispatch),
});

class AddPlayerScreen extends Screen<any> {

    state = {
        name: 'Aarón',
        number: '1',
        position: 'Delantero'
    };

    // componentWillMount(): void {
    //     this.props.teamActions.getPlayersClub(this.props.match.params.id);
    // }

    savePlayer = () => {
        const playerData = {
            ...this.state,
            number: parseInt(this.state.number),
            club_id: this.props.match.params.id,
        };
        this.props.addPlayerActions.savePlayer(playerData);
    };

    private isFormValid(): boolean {
        if (!this.state.name || this.state.name.length === 0) {
            return false;
        }
        if (!this.state.number || parseInt(this.state.number) <= 0 || parseInt(this.state.number) > 99) {
            return false;
        }
        if (!this.state.position || this.state.position.length === 0) {
            return false;
        }
        return true;
    }

    public renderScreen(): React.ReactNode {
        return (
            <div className="container">

                <h1>Añadir Jugador</h1>

                <div>
                    <label>Nombre</label>
                    <input type={'text'} value={this.state.name} onChange={(event) => this.setState({name: event.target.value})} />
                    <br/>

                    <label>Dorsal</label>
                    <input type={'number'} value={this.state.number} onChange={(event) => this.setState({number: event.target.value})} />
                    <br/>

                    <label>Posición</label>
                    <input type={'text'} value={this.state.position} onChange={(event) => this.setState({position: event.target.value})} />
                    <br/>

                    <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        disabled={!this.isFormValid()}
                        onClick={this.savePlayer}
                    >Guardar</button>

                </div>

            </div>
        );
    }
}



export default connect(null, mapDispatchToProps as any)(AddPlayerScreen);