import axios from "axios";
import {URL_WS_CLUBS} from "../../ws/Endpoint";

export const GET_CLUBS_DATA = 'get-clubs-data';
export const GET_CLUBS_ERROR = 'get-clubs-error';

export const getClubs = () => (dispatch: any) => {

    axios.get(URL_WS_CLUBS)
        .then(res => {
            dispatch({ type: GET_CLUBS_DATA, payload: res.data });
        })
        .catch((err) => {
            dispatch({ type: GET_CLUBS_ERROR, payload: 'Se ha producido un error obteniendo información' });
        });
};