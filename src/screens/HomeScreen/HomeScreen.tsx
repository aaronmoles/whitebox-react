import * as React from "react";
import Screen from "../Screen.tsx";
import {bindActionCreators} from "redux";
import * as homeActions from './HomeActions';
import {connect} from "react-redux";
import Club from "../../Entity/Club";
import {Link} from "react-router-dom";
import {generateRoute} from "../../routing/Routing";
import {ROUTE_TEAM} from "../../routing/Routes";

const mapStateToProps = (state: any) => ({
    clubs: state.HomeReducer.clubs,
});

const mapDispatchToProps = (dispatch: any) => ({
    homeActions: bindActionCreators(homeActions as any, dispatch),
});

class HomeScreen extends Screen<any> {

    componentWillMount(): void {
        this.props.homeActions.getClubs();
    }

    public renderScreen(): React.ReactNode {
        return (
            <div className="container">

                <h1>Clubes</h1>
                <div className="table-responsive">
                    {this.renderContent()}
                </div>


            </div>
        );
    }

    private renderContent(): React.ReactNode {
        if (this.props.error) {
            return <h3>{this.props.error}</h3>
        }
        return (
            <table className="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Ciudad</th>
                    <th>Detalle</th>
                </tr>
                </thead>
                <tbody>
                {this.props.clubs.map((club: Club, index: number) => {
                    return (
                        <tr key={index}>
                            <td>{club.id}</td>
                            <td>{club.name}</td>
                            <td>{club.city}</td>
                            <td>
                                <Link to={generateRoute(ROUTE_TEAM, club.id)}>
                                    <button type="button" className="btn btn-primary btn-sm">Ver jugadores</button>
                                </Link>
                            </td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        );
    }
}



export default connect(mapStateToProps, mapDispatchToProps as any)(HomeScreen);