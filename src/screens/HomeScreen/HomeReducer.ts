import {Action} from "../../redux/Action";
import {GET_CLUBS_DATA, GET_CLUBS_ERROR} from "./HomeActions";

const INITIAL_STATE = {
    clubs: [],
    error: null,
};

export function homeReducer(state = INITIAL_STATE, action: Action<any>) {
    switch (action.type) {
        case GET_CLUBS_DATA:
            return Object.assign({}, state, {
                clubs: action.payload,
                error: null,
            });
        case GET_CLUBS_ERROR:
            return Object.assign({}, state, {
                clubs: [],
                error: action.payload,
            });
        default:
            return state
    }
}