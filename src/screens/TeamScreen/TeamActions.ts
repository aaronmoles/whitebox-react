import axios from "axios";
import {URL_WS_TEAM_PLAYERS} from "../../ws/Endpoint";
import {generateRoute} from "../../routing/Routing";

export const GET_PLAYERS_DATA = 'get-players-data';
export const GET_PLAYERS_ERROR = 'get-players-error';

export const getPlayersClub = (clubId: number) => (dispatch: any) => {

    axios.get(generateRoute(URL_WS_TEAM_PLAYERS, clubId))
        .then(res => {
            dispatch({ type: GET_PLAYERS_DATA, payload: res.data });
        })
        .catch((err) => {
            dispatch({ type: GET_PLAYERS_ERROR, payload: 'Se ha producido un error obteniendo información' });
        });
};