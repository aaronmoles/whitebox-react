import {Action} from "../../redux/Action";
import {GET_PLAYERS_DATA, GET_PLAYERS_ERROR} from "./TeamActions";

const INITIAL_STATE = {
    players: [],
    error: null,
};

export function teamReducer(state = INITIAL_STATE, action: Action<any>) {
    switch (action.type) {
        case GET_PLAYERS_DATA:
            return Object.assign({}, state, {
                players: action.payload,
                error: null,
            });
        case GET_PLAYERS_ERROR:
            return Object.assign({}, state, {
                players: [],
                error: action.payload,
            });
        default:
            return state
    }
}