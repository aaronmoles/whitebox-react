import * as React from "react";
import Screen from "../Screen.tsx";
import {bindActionCreators} from "redux";
import * as teamActions from './TeamActions';
import {connect} from "react-redux";
import Player from "../../Entity/Player";
import {generateRoute} from "../../routing/Routing";
import {ROUTE_ADD_PLAYER, ROUTE_TEAM} from "../../routing/Routes";
import {Link} from "react-router-dom";

const mapStateToProps = (state: any) => ({
    players: state.TeamReducer.players,
});

const mapDispatchToProps = (dispatch: any) => ({
    teamActions: bindActionCreators(teamActions as any, dispatch),
});

class TeamScreen extends Screen<any> {

    componentWillMount(): void {
        this.props.teamActions.getPlayersClub(this.props.match.params.id);
    }

    public renderScreen(): React.ReactNode {
        return (
            <div className="container">

                <h1>Bootstrap grid examples</h1>
                <Link to={generateRoute(ROUTE_ADD_PLAYER, this.props.match.params.id)}>
                    <button type="button" className="btn btn-primary btn-sm">Añadir jugador</button>
                </Link>
                <div className="table-responsive">
                    <table className="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Número</th>
                            <th>Posición</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.players.map((player: Player, index: number) => {
                            return (
                                <tr key={index}>
                                    <td>{player.id}</td>
                                    <td>{player.name}</td>
                                    <td>{player.number}</td>
                                    <td>{player.position}</td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>


            </div>
        );
    }
}



export default connect(mapStateToProps, mapDispatchToProps as any)(TeamScreen);