const base = "";


export const ROUTE_HOME = base + "/";
export const ROUTE_TEAM = base + "/team/:id";
export const ROUTE_ADD_PLAYER = base + "/player/add/:id";