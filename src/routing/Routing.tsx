import * as  React from "react";
import {ReactNode} from "react";
import {RouterProps} from "react-router";
import {Route, Router} from "react-router-dom";
import {ROUTE_ADD_PLAYER, ROUTE_HOME, ROUTE_TEAM} from "./Routes";
import HomeScreen from "../screens/HomeScreen/HomeScreen";
import TeamScreen from "../screens/TeamScreen/TeamScreen";
import AddPlayerScreen from "../screens/AddPlayerScreen/AddPlayerScreen";

export default class Routing extends React.Component<RouterProps> {

    public render(): ReactNode {
        return(
            <Router history={this.props.history}>
                <div>
                    <Route path={ROUTE_HOME} exact component={HomeScreen}/>
                    <Route path={ROUTE_TEAM} exact component={TeamScreen}/>
                    <Route path={ROUTE_ADD_PLAYER} exact component={AddPlayerScreen}/>
                </div>
            </Router>
        );
    }

}

export function generateRoute(route: string, id?: number): string{
    let routeStr = route;
    if (id) {
        routeStr = routeStr.replace(String(':id'), String(id));
    }
    return routeStr;
}
