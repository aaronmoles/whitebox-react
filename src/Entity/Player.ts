export default interface Player {
    id?: number,
    name: string,
    position: string,
    number: number,
    club_id?: number,
}