export default interface Club {
    id: number,
    name: string,
    city: string,
}