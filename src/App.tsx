import React, {Component} from 'react';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import reducers from "./redux/combineReducers";
import Routing from "./routing/Routing";
import {routerMiddleware} from "react-router-redux";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";


class App extends Component {

    protected history: any;

    constructor(props: any, context: any) {
        super(props, context);
        this.history = createHistory();
    }

    render() {
        const middleware = routerMiddleware(this.history);
        const store = createStore(reducers, applyMiddleware(thunk, middleware));
        return (
            <Provider store={store}>
                <Routing history={this.history}/>
            </Provider>
        );
    }
}

export default App;
