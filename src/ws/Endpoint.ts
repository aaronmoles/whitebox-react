const BASE = 'http://localhost:8000';

export const URL_WS_CLUBS = `${BASE}/club/`;
export const URL_WS_TEAM_PLAYERS = `${BASE}/club/players/:id`;
export const URL_WS_ADD_PLAYER = `${BASE}/player/add`;