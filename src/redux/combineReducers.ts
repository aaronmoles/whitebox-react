import { combineReducers } from 'redux'
import {homeReducer} from "../screens/HomeScreen/HomeReducer";
import {teamReducer} from "../screens/TeamScreen/TeamReducer";

const reducers = combineReducers({
    HomeReducer: homeReducer,
    TeamReducer: teamReducer,
});

export default reducers;